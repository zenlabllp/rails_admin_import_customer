# app/workers/hard_worker.rb
class MailchimpWorker
  include Sidekiq::Worker

  def perform(list)
    Gibbon::API.api_key = ENV['MAILCHIMP_KEY']
    lists = Gibbon::API.lists.list({:start => 0, :limit => 10})
    list_id = lists['data'].select { |list| list['name'] == 'DSC World Newsletter' }.first['id']
    res = Gibbon::API.lists.batch_subscribe(:id => list_id, batch: list, :double_optin => true)
    p res
  end
end