# app/workers/hard_worker.rb
class CustomerWorker
  include Sidekiq::Worker

  def perform(r)
    item = nil
    item = Customer.find_by_email(r['email'])

    if item.nil?
      item = Customer.new(email: r['email'])
      item.save
    end

    r['customer_line_items'].each do |l|
      if !l['serial_no'].empty?
        line_item = item.customer_line_items.where(serial_no: l['serial_no']).first
        if line_item
          line_item.update_attributes(l)
          line_item.save
        else
          item.customer_line_items.build(l)
          item.save
        end
      end
    end

    if !item.is_subscribed
      item.is_subscribed = r['news_info']
    end
    item.source = 'Import'
    item.save
    item
  end
end