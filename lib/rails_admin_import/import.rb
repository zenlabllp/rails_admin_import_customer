require 'open-uri'
require "rails_admin_import/import_logger"

module RailsAdminImport
  module Import
    extend ActiveSupport::Concern

    module ClassMethods
      def file_fields
        attrs = []
        if self.methods.include?(:attachment_definitions) && !self.attachment_definitions.nil?
          attrs = self.attachment_definitions.keys
        end
        attrs - RailsAdminImport.config(self).excluded_fields
      end

      def import_fields
        fields = []

        fields = self.new.attributes.keys.collect { |key| key.to_sym }

        self.belongs_to_fields.each do |key|
          fields.delete("#{key}_id".to_sym)
        end

        self.file_fields.each do |key|
          fields.delete("#{key}_file_name".to_sym)
          fields.delete("#{key}_content_type".to_sym)
          fields.delete("#{key}_file_size".to_sym)
          fields.delete("#{key}_updated_at".to_sym)
        end

        excluded_fields = RailsAdminImport.config(self).excluded_fields
        [:id, :created_at, :updated_at, excluded_fields].flatten.each do |key|
          fields.delete(key)
        end

        fields
      end

      def belongs_to_fields
        attrs = self.reflections.select { |k, v| v.macro == :belongs_to }.keys
        attrs - RailsAdminImport.config(self).excluded_fields
      end

      def many_fields
        attrs = []
        self.reflections.each do |k, v|
          if [:has_and_belongs_to_many, :has_many].include?(v.macro)
            attrs << k.to_s.singularize.to_sym
          end
        end

        attrs - RailsAdminImport.config(self).excluded_fields
      end

      def open_spreadsheet(file)
        case File.extname(file.original_filename)
        when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
        when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
        end
      end

      def run_import(params)
        logger     = ImportLogger.new

        begin
          if !params.has_key?(:file)
            return results = { :success => [], :error => ["You must select a file."] }
          end

          copy_import_file_to_logs(params[:file].tempfile, logger) if RailsAdminImport.config.logging

          file = open_spreadsheet(params[:file])
          if file.last_row > RailsAdminImport.config.line_item_limit
            return results = { :success => [], :error => ["Please limit upload file to #{RailsAdminImport.config.line_item_limit} line items."] }
          end

          map = {}
          file.row(1).each_with_index do |key, i|
            key = key.gsub(/ /, '_').gsub(/\./, '').downcase
            if self.many_fields.include?(key.to_sym)
              map[key.to_sym] ||= []
              map[key.to_sym] << i
            else
              map[key.to_sym] = i
            end
          end
          update = :email

          if update && !map.has_key?(update)
            return results = { :success => [], :error => ["Your file must contain a column for the 'Update lookup field' you selected."] }
          end

          results = { :success => [], :error => [] }

          associated_map = {}

          label_method = RailsAdminImport.config(self).label

          @index = 0
          list = []
          rows = []
          file.each do |row|
            if @index == 0
              @index += 1
              next
            end

            row = row.map { |val| val.to_s }
            new_attrs = {}
            self.import_fields.each do |key|
              new_attrs[key] = row[map[key]] if map[key]
            end

            r = (rows.select { |r| r['email'] == row[map['email'.to_sym]] }).first
            if !r
              r = { }
              r['news_info'] = row[map['news_info'.to_sym]]
              r['email'] = row[map['email'.to_sym]]
              r['news_info'] = row[map['news_info'.to_sym]]
              r['customer_line_items'] = []
              rows << r
            end

            if row[map['serial_no'.to_sym]]
              r['customer_line_items'] << {serial_no: row[map['serial_no'.to_sym]], product_purchased: row[map['product_purchased'.to_sym]], user_name: row[map['user_name'.to_sym]], phone_no: row[map['phone_no'.to_sym]], state: row[map['state'.to_sym]], registration_date: row[map['reg_date'.to_sym]].gsub('+00:00', '+08:00')}
            end

            list << {email: {email: row[map[:email]]}, :merge_vars => {:FNAME => '', :LNAME => ''}}
            @index += 1
          end

          results
        rescue Exception => e
          logger.info "#{Time.now.to_s}: Unknown exception in import: #{e.inspect}"
          results[:error] << "Could not upload. Unexpected error: #{e.to_s}"
        end
        rows.each do |r|
          CustomerWorker.perform_async(r)
        end
        MailchimpWorker.perform_async(list)
        results
      end

      def copy_import_file_to_logs(import_file, logger)
        import_file_path = "#{Rails.root}/log/import/"
        import_file_name = "#{Time.now.strftime("%Y-%m-%d-%H-%M-%S")}-import.csv"

        FileUtils.mkdir_p import_file_path
        FileUtils.copy(import_file, tempfile,import_file_path+import_file_name)
        rescue Exception => e
          logger.info "#{Time.now.to_s}: can't copy import file to logs: #{e.inspect}"
      end
    end

    def before_import_save(*args)
      # Meant to be overridden to do special actions
    end

    def import_display
      self.id
    end

    def import_files(row, map)
      if self.new_record? && self.valid?
        self.class.file_fields.each do |key|
          if map[key] && !row[map[key]].nil?
            begin
              # Strip file
              row[map[key]] = row[map[key]].gsub(/\s+/, "")
              format = row[map[key]].match(/[a-z0-9]+$/)
              open("#{Rails.root}/tmp/#{self.permalink}.#{format}", 'wb') { |file| file << open(row[map[key]]).read }
              self.send("#{key}=", File.open("#{Rails.root}/tmp/#{self.permalink}.#{format}"))
            rescue Exception => e
              self.errors.add(:base, "Import error: #{e.inspect}")
            end
          end
        end
      end
    end

    def import_belongs_to_data(associated_map, row, map)
      self.class.belongs_to_fields.each do |key|
        if map.has_key?(key) && row[map[key]] != ""
          self.send("#{key}_id=", associated_map[key][row[map[key]]])
        end
      end
    end

    def import_many_data(associated_map, row, map)
      self.class.many_fields.each do |key|
        values = []

        map[key] ||= []
        map[key].each do |pos|
          if row[pos] != "" && associated_map[key][row[pos]]
            values << associated_map[key][row[pos]]
          end
        end

        if values.any?
          self.send("#{key.to_s.pluralize}=", values)
        end
      end
    end
  end
end

class ActiveRecord::Base
  include RailsAdminImport::Import
end
